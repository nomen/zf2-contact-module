<?php
/**
 * Base Configuration for the Net Glue Contact Module
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */
return array(
	
	'netglue_contact' => array(
		'form' => array(
			'name' => 'contact',
			'attributes' => array(
				'class' => 'form-horizontal',
			),
			'filter_config' => array(
				array(
					'name' => 'fromName',
					'validators' => array(
						array(
							'name' => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min' => 2,
								'max' => 140,
							),
						),
					),
				),
			),
			'captcha' => array(
				'enable' => true,
				'class' => 'dumb',
			),
		),
		'responder' => array(
			'email' => array(
				'enable' => true,
				'send_receipt' => true,
				'recipient' => 'me@example.com',
				'sender' => 'Fred <fred@example.com>',
				'template' => 'mail/contact-message',
				'receipt_template' => 'mail/receipt',
				'subject' => 'Website Contact',
				'receipt_subject' => 'Thank you for your message',
			),
		),
		'observers' => array(
			NetglueContact\Controller\ContactController::EVENT_RENDER_FORM => array(
				
			),
			NetglueContact\Controller\ContactController::EVENT_CONTACT => array(
				'NetglueContact\Service\ContactResponder' => 'respond',
			),
		),
	),
	
	/**
	 * Location of Email Message Templates
	 * Supply a different directory or map to overrirde the default email templates
	 * Take a look at the NetglueMtmail module for more information
	 */
	'NetglueMtmail\Service' => array(
		'template_map' => array(),
		'template_path_stack' => array(
			'netglue-contact' => __DIR__ . '/../view/netglue-contact',
		),
	),
	
	/**
	 * We will need access to a mail transport to send messages if enabled
	 * Naturally, this uses the Transport Factory in the Netglue libs
	 * Note that it is also entered in [service_manager][factories]
	 * @see \Netglue\Mail\Service\TransportFactory
	 */
	'mail_transport' => array(
		'class' => 'sendmail',
		'options' => NULL, // Smtp Options, File Options etc...
	),
	
	/**
	 * Controllers
	 */
	'controllers' => array(
		'factories' => array(
			'ContactController' => 'NetglueContact\Service\ContactControllerFactory',
		),
	),
	
	'service_manager' => array(
		'factories' => array(
			// For creating a ready configured form
			'NetglueContactForm' => 'NetglueContact\Service\ContactFormFactory',
			// Making sure our mail transport service is available to the service locator
			'MailTransport' => 'Netglue\Mail\Service\TransportFactory',
			// The observer is added here because it needs to be injected with a service locator
			'NetglueContact\Service\ContactResponder' => 'NetglueContact\Service\ContactResponder',
			// For returning Captcha instances for use with the form
			'NetglueContactCaptcha' => 'NetglueContact\Service\CaptchaFactory',
		),
	),
	
	/**
	 * Routes...
	 * Override in local config to set different URLs
	 */
	'router' => array(
		'routes' => array(
			// The array key 'contact_form' is used to refer to the 'bundle' of routes as a whole
			'contact_form' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/contact', // Change this to alter the end point of the contact controller
					'defaults' => array(
						__NAMESPACE__ => 'NetglueContact\Controller',
						'controller' => 'ContactController',
						'action' => 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array( // To refer to this route, we use 'contact_form/default' - i.e. parent-key/child-route-key
						'type' => 'Segment',
						'options' => array(
							'route' => '/:action',
							'constraints' => array(
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
							),
						),
					),
				),
			),
		),
	),
	
	
	'view_manager' => array(
		'template_map' => array(
			'netglue-contact/contact/index'     => __DIR__ . '/../view/netglue-contact/contact/index.phtml',
			'netglue-contact/contact/thank-you'     => __DIR__ . '/../view/netglue-contact/contact/thank-you.phtml',
		),
		'template_path_stack' => array(
			'netglue-contact' => __DIR__ . '/../view', // Files in this directory are divided by /module-name/controller-name/action-name.phtml (Drop the 'Controller' part - i.e. FooController becomes 'foo')
		),
	),
	
);
