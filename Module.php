<?php
/**
 * Netglue Contact Module
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */

namespace NetglueContact;

use Zend\EventManager\EventInterface as Event;

/**
 * Netglue Contact Module
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */
class Module {
	
	/**
	 * Return autoloader configuration
	 * @link http://framework.zend.com/manual/2.0/en/user-guide/modules.html
	 * @return array
	 */
	public function getAutoloaderConfig() {
		return array(
			'Zend\Loader\ClassMapAutoloader' => array(
				__DIR__ . '/autoload_classmap.php'
			),
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
				),
			),
		);
	}
	
	/**
	 * Include/Return module configuration
	 * @return array
	 */
	public function getConfig() {
		return include __DIR__ . '/config/module.config.php';
	}
	
	
	/**
	 * MVC Bootstrap Event
	 *
	 * This method scans the module configuration for any callbacks that want
	 * to attach to events triggered by the contact controller.
	 * Each array element is keyed by event name and should contain an array of
	 * callbacks to attach to that event
	 *
	 * Is it good/bad practice to attach/configure observers on bootstrap?
	 * Is autoloading all setup now? Probably...
	 *
	 * @param Event $e
	 * @return void
	 */
	public function onBootstrap(Event $e) {
		$app = $e->getApplication();
		$serviceLocator = $app->getServiceManager();
		$evtMgr = $serviceLocator->get('SharedEventManager');
		$config = $serviceLocator->get('config');
		if(isset($config['netglue_contact']['observers'])) {
			$config = $config['netglue_contact']['observers'];
			foreach($config as $eventName => $observers) {
				$evtMgr->attach('NetglueContact\Controller\ContactController', $eventName, function(Event $e) use ($serviceLocator, $observers) {
					foreach($observers as $class => $method) {
						if(is_callable($method)) {
							call_user_func_array($method, array($e));
							continue;
						}
						$instance = $serviceLocator->get($class);
						$instance->{$method}($e);
					}
				});
			}
		}
	}
	
}
