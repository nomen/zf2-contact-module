<?php
/**
 * Netglue Contact Module Form Input Filter
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */

namespace NetglueContact\Form;

use Zend\InputFilter\InputFilter;
use Zend\Validator\Hostname as HostnameValidator;

/**
 * Netglue Contact Module Form Input Filter
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */
class ContactFilter extends InputFilter {
	
	/**
	 * Construct
	 * @return void
	 */
	public function __construct() {
		
		/**
		 * Sender Name
		 */
		$this->add(array(
			'name' => 'fromName',
			'required'   => true,
			'validators' => array(),
			'filters' => array(
				array(
					'name' => 'StringTrim'
				),
			),
		));
		
		/**
		 * Email Address
		 */
		$this->add(array(
			'name' => 'email',
			'required' => true,
			'validators' => array(
				array(
					'name' => 'EmailAddress',
					'options' => array(
						'useMxCheck' => true,
						'allow' => HostnameValidator::ALLOW_DNS,
						'useDomainCheck' => true,
					),
				),
			),
			'filters' => array(
				array(
					'name' => 'StringTrim'
				),
				array(
					'name' => 'StringToLower'
				),
			),
		));
		
		/**
		 * Message Body
		 */
		$this->add(array(
			'name' => 'message',
			'required' => true,
			'validators' => array(
				array(
					'name' => 'StringLength',
					'options' => array(
						'encoding' => 'UTF-8',
						'min' => 2,
						'max' => NULL,
					),
				),
			),
		));
		
	}
	
}
