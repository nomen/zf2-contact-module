<?php
/**
 * A Factory to create a contact controller instance
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */

namespace NetglueContact\Service;

/**
 * To instantiate a controller
 */
use NetglueContact\Controller\ContactController;

/**
 * To implement factory interface
 */
use Zend\ServiceManager\FactoryInterface;

/**
 * To accept Service Locator Objects
 */
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * To Create a Form
 */
use NetglueContact\Form\ContactForm;

/**
 * A Factory to create a contact controller instance
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */
class ContactControllerFactory implements FactoryInterface {
	
	/**
	 * Create Service, Return a fresh contact controller with everything required setup for use
	 * @return NetglueContact\Controller\ContactController
	 * @param ServiceLocatorInterface $services
	 */
	public function createService(ServiceLocatorInterface $services) {
		
		$serviceLocator = $services->getServiceLocator();
		
		/**
		 * Retrieve the contact form from the service locator
		 * In module config, we have $config['service_manager']['factories']['NetglueContactForm'] => 'NetglueContact\Service\ContactFormFactory'
		 * which is responsible for creating and configuring our form.
		 * The key is the name of the service
		 */
		$form = $serviceLocator->get('NetglueContactForm');
		
		$config  = $serviceLocator->get('config');
		if($config instanceof Traversable) {
			$config = ArrayUtils::iteratorToArray($config);
		}
		
		$controller = new ContactController;
		$controller->setContactForm($form);
		return $controller;
	}
	
	
}